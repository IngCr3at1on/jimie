using Gtk;

/// Functions used by the menu that don't belong anywhere else.
public class JimMenuFunctions {
	public JimMenuFunctions() {}

	// Generate an about dialog.
	public void post_about() {
		AboutDialog dialog = new AboutDialog();
		dialog.set_destroy_with_parent(true);
		//dialog.set_transient_for(window);
		dialog.set_modal(true);

		dialog.authors = {"Nathan Bass"};
		dialog.program_name = "JIMIE";
		dialog.comments = "The JIM Initial Experiment client";
		dialog.copyright = "Copyright (c) 2013, Nathan Bass";
		dialog.version = "0.0.1";
		
		dialog.license = "The MIT License (MIT)";
		//dialog.wrap_license = true;

		dialog.response.connect ((response_id) => {
			if(response_id == Gtk.ResponseType.CANCEL || response_id == Gtk.ResponseType.DELETE_EVENT) {
				dialog.hide_on_delete ();
			}
		});

		// Show the dialog.
		dialog.present();
	} // void post_about
} // class JimMenuFunctions
