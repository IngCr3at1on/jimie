using Gtk;

/**
 * Generate a tabbed notebook and open the server status page.
 */
public class JimWindow : Grid {
	// Initialize our menu object with the Notebook object.
	public JimWindow() {
		// Generate our notebook (do this early so we have it to reference).
		JimNotebook jim_notebook = new JimNotebook();
		// Load our menu_functions into memory for reference.
		JimMenuFunctions menu_functions = new JimMenuFunctions();

		// Create a menu bar to attach our menu items to.
		MenuBar menubar = new MenuBar();

		/* Both Menu and MenuItem exist in glib as well as gtk, so we define
		 * which to use. */
		Gtk.MenuItem item_main = new Gtk.MenuItem.with_label("Jim");
		Gtk.Menu mainmenu = new Gtk.Menu();
		item_main.set_submenu(mainmenu);
		menubar.add(item_main);

			Gtk.MenuItem item_close = new Gtk.MenuItem.with_label("Close");
			mainmenu.add(item_close);
			item_close.activate.connect(()=> {
				jim_notebook.notebook_close_tab();
			});

			Gtk.MenuItem item_exit = new Gtk.MenuItem.with_label("Exit");
			mainmenu.add(item_exit);
			item_exit.activate.connect(()=> {
				// TODO: Close all tabs and quit.
			});

		// Also generate a help menu.
		Gtk.MenuItem item_help = new Gtk.MenuItem.with_label("Help");
		Gtk.Menu helpmenu = new Gtk.Menu();
		item_help.set_submenu(helpmenu);
		menubar.add(item_help);

			Gtk.MenuItem item_about = new Gtk.MenuItem.with_label("About");
			helpmenu.add(item_about);
			item_about.activate.connect(()=> {
				menu_functions.post_about();
			});

		// Attach the newly populated menubar to our window.
		this.attach(menubar, 0, 0, 1, 1);

		/* Attach the newly generated notebook to our grid (underneath the
		 * menubar). */
		this.attach_next_to(
			jim_notebook,
			menubar,
			PositionType.BOTTOM,
			1,
			1
		);

		// Open an empty tab (testing).
		jim_notebook.notebook_open_tab("testing");
	} // public JimWindow
} // class JimWindow

/**
 * Generate a window with a grid housing our chat boxes.
 * 
 * TODO
 * 		Set tabs to be detacheable.
 * 		Add a statusbar.
 * 		Add settings to disable toolbar and menu.
 */
public class Application : Window {
	public Application() {
		// Setup our main window properties.
		this.title = "JIMIE";
		this.border_width = 1; // 1 + 1 added by the notebook.
		this.window_position = WindowPosition.CENTER;
		this.set_default_size(650, 725);
		this.destroy.connect(main_quit);

		JimWindow jim = new JimWindow();
		this.add(jim);

		this.show_all();
	} // public Application

	public static int main(string[] args) {
		init(ref args);

		Application jimie = new Application();
		jimie.show_all();

		Gtk.main();
		return 0;
	} // public main

} // class Application
