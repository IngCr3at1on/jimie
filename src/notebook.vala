using Gtk;

/**
 * Extend the standard gtk notebook for our own usage.
 */
public class JimNotebook : Notebook {
	public JimNotebook() {
		this.set_scrollable(true);
	}

	// Generate a new notebook tab with a scrollable window housed inside of it.
	public void notebook_open_tab(string? origin, bool focus = false) {
		/* Create a widget and set it up to be a close button using the stock
		 * gtk icons. */
		Button btn = new Button();
		btn.set_focus_on_click(false);
		btn.set_name("close-tab-button");

		Image image = new Image();
		image.set_from_icon_name("gtk-close", IconSize.MENU);
		btn.add(image);

		// Also create a widget to hold the text label for our new tab.
		Label label_txt = new Label("");
		if(origin != null) {
			label_txt.set_text(origin);
		}

		// Generate a grid and place both our tab label and close button inside.
		Grid tab_grid = new Grid();
		tab_grid.attach(label_txt, 0, 0, 1, 1);
		tab_grid.attach_next_to(btn, label_txt, PositionType.RIGHT, 1, 1);

		/* If we don't display the grid it won't show our title and close button
		 * with the rest of the window. */
		tab_grid.show_all();

		// Generate a scrollable window and attach our output.
		ScrolledWindow output = new ScrolledWindow(null, null);
		output.set_policy(
			PolicyType.AUTOMATIC,
			PolicyType.AUTOMATIC
		);
		output.border_width = 1;

		/* Make sure the scrolled_window is set to fill the entire non-occupied
		 * area of the window. */
		output.set_hexpand(true);
		output.set_halign(Align.FILL);
		output.set_vexpand(true);
		output.set_valign(Align.FILL);

		// Set a background color for our output window.
		Gdk.RGBA bg = Gdk.RGBA();
		bg.red = 192;
		bg.green = 192;
		bg.blue = 192;
		output.override_background_color(StateFlags.NORMAL, bg);

		/* Provide an input box in the form of a scrollable window, same as our
		 * output box. */
		ScrolledWindow input = new ScrolledWindow(null, null);
		input.set_policy(
			PolicyType.AUTOMATIC,
			PolicyType.AUTOMATIC
		);
		input.border_width = 1;

		// Fill the entire lower section of the window same as the output box.
		input.set_hexpand(true);
		input.set_halign(Align.FILL);

		// Add a text view to our input box to allow typing.
		TextView view = new TextView();
		view.set_wrap_mode(WrapMode.WORD);
		input.add(view);

		/* Generate a grid to house both our scrollable chat output and an
		 * input box. */
		Grid main_grid = new Grid();
		main_grid.attach(output, 0, 0, 1, 1);
		main_grid.attach_next_to(
			input,
			output,
			PositionType.BOTTOM,
			1,
			1
		);

		// Display the newly configured grid and it's contents.
		main_grid.show_all();

		// Finally attach our tab to the main notebook so we can see and use it.
		this.append_page(main_grid, tab_grid);
		this.set_tab_reorderable(main_grid, true);

		/* Add a callback function for the close button, has to be defined after
		 * main grid. */
		btn.clicked.connect(() => {
			notebook_close_tab(page_num(main_grid));
		});

		// Change our focus to the new tab (only if this is called).
		if(focus == true) {
			this.set_current_page(page_num(main_grid));
		}
	} // void notebook_open_tab

	/* Close a tab. If no tab number is given it will remove the tab with focus.
	 * 
	 * TODO
	 * 		Add close confirmation dialog.
	 */
	public void notebook_close_tab(int tab = -99) {
		if(tab == -99) tab = this.get_current_page();
		this.remove_page(tab);
	} // void notebook_close_tab
} // class JimNotebook
